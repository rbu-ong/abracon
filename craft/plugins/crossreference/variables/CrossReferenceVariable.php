<?php
namespace Craft;

class CrossReferenceVariable
{
    public function search($partNum, $page = 1)
    {
        return craft()->crossReference->searchPartNum($partNum, $page);
    }
}
