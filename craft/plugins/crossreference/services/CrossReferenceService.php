<?php
namespace Craft;

class CrossReferenceService extends BaseApplicationComponent
{
    public function searchPartNum($partNum, $page)
    {
        return CrossReference_RowModel::populateModels($this->se_search($partNum, $page));
    }

    private function se_search($partNum, $page)
    {
      $url = "https://app.siliconexpert.com/SearchService/search/xref";
      $params = [
        'parts' => '[{"partNumber":"' . $partNum . '"}]',
        'pageNumber' => $page
      ];

      $response = $this->get_request($url, $params);
      $part_rows = $response['Result']['CrossData']['CrossDto'];
      // The CrossDto isn't an array if just there is just one result and we need it to be
      return (is_array($part_rows) && array_key_exists('ComID', $part_rows)) ? [$part_rows] : $part_rows;
    }

    private function get_request($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, "");
        curl_setopt($ch, CURLOPT_URL, "https://app.siliconexpert.com/SearchService/search/authenticateUser?login=Abracon_Trial&apiKey=857139481531");
        $authenticate = curl_exec($ch);
        if ($this->error_state(json_decode($authenticate, true))) return false;

        curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($params));
        $response = curl_exec($ch);

        $response = json_decode($response, true);
        if ($this->error_state($response)) return false;

        curl_close($ch);
        return $response;
    }

    private function error_state($response)
    {
        if ($response['Status']['Success'] != 'true')
        {
          CrossReferencePlugin::log($response['Status']['Message'], LogLevel::Error);
          return true;
        } else {
          return false;
        }
    }


}
