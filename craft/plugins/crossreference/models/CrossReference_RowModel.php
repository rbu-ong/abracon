<?php

namespace Craft;

class CrossReference_RowModel extends BaseModel
{
    /**
     * Defines what is returned when someone puts {{ ingredient }} directly
     * in their template.
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->attributes);
    }

    /**
     * Define the attributes this model will have.
     *
     * @return array
     */
    public function defineAttributes()
    {
        return array(
            'ComID' => AttributeType::String,
            'PartNumber' => AttributeType::String,
            'Manufacturer' => AttributeType::String,
            'Lifecycle' => AttributeType::String,
            'Datasheet' => AttributeType::String,
            'Description' => AttributeType::String,
            'RoHSStatus' => AttributeType::String,
            'CrossID' => AttributeType::String,
            'CrossPartNumber' => AttributeType::String,
            'CrossManufacturer' => AttributeType::String,
            'CrossLifecycle' => AttributeType::String,
            'CrossDatasheet' => AttributeType::String,
            'CrossDescription' => AttributeType::String,
            'CrossRoHSStatus' => AttributeType::String,
            'PlName' => AttributeType::String,
            'Type' => AttributeType::String,
            'Comment' => AttributeType::String
        );
    }
}
