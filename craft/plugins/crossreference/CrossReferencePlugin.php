<?php
namespace Craft;

class CrossReferencePlugin extends BasePlugin
{
    function getName()
    {
         return Craft::t('Cross Reference');
    }

    function getVersion()
    {
        return '0.1';
    }

    function getDeveloper()
    {
        return 'Masonry';
    }

    function getDeveloperUrl()
    {
        return 'https://builtbymasonry.com';
    }
}
