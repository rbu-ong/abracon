<?php
namespace Craft;

class ACPostmanRecord extends BaseRecord
{
    /**
     * Returns the name of the associated database table.
     *
     * @return string
     */
    public function getTableName()
    {
        return 'acpostman_emails';
    }

    /**
     * Defines this model's relations to other models.
     *
     * @return array
     */
    public function defineRelations()
    {
        return [
            'element' => [static::BELONGS_TO, 'ElementRecord', 'required' => true, 'onDelete' => static::CASCADE],
            'creator' => [static::BELONGS_TO, 'UserRecord'],
            'template' => [
                static::BELONGS_TO,
                'ACPostman_TemplateRecord',
                'onDelete' => static::RESTRICT,
            ]
        ];
    }

    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    public function defineAttributes()
    {
        return [
            'status' => [AttributeType::Enum,
                'values' => [
                        'sent',
                        'pending'
                    ],
                'default' => 'pending',
            ]
        ];
    }
}
