<?php

namespace Craft;

class ACPostman_TemplateController extends BaseController
{
    public function actionIndex()
    {
        $variables['template'] = craft()->aCPostman_template->getAll(['order' => 'sortOrder']);

        return $this->renderTemplate('acpostman/templates', $variables);
    }

    public function actionEdit(array $variables = [])
    {

        if (empty($variables['emailTemplate'])) {
            if (!empty($variables['templateId'])) {
                $variables['emailTemplate'] = craft()->aCPostman_template->find($variables['templateId']);
                
                if (!$variables['emailTemplate']) {
                    throw new HttpException(404);
                }
            } else {
                $variables['emailTemplate'] = new ACPostman_TemplateModel();
            }
        }
        
        # load RichTextField sources
        craft()->templates->includeCssResource('lib/redactor/redactor.css');
        craft()->templates->includeJsResource('lib/redactor/redactor.js');
        craft()->templates->includeJsResource('lib/redactor/plugins/fullscreen.js');
        craft()->templates->includeJsResource('lib/redactor/plugins/source.js');
        craft()->templates->includeJsResource('js/RichTextInput.js');

        $this->renderTemplate('acpostman/_addTemplate', $variables);
    }

    public function actionSave()
    {
        $this->requirePostRequest();

        $postData = craft()->request->getPost();

        if ($id = craft()->request->getPost('templateId'))
        {
            $template = craft()->aCPostman_template->find($id);

            if (!$template)
            {
                throw new Exception(Craft::t('No email template exists with ID --> "{id}"', [
                    'id' => $id
                ]));
            }
        }
        else
        {
            $template = new ACPostman_TemplateModel();
        }

        if( craft()->aCPostman_template->checkDuplicateTitle( craft()->request->getPost('title', $template->title), craft()->request->getPost('templateId') ) ){
            craft()->userSession->setError(Craft::t('Template title already exist.'));

            // Send the email back to the template
            craft()->urlManager->setRouteVariables(array(
                'template' => $template
            ));

            return;
        }

        // store custom fields data
        $template->setContentFromPost('fields');

        // Set creator and updater
        $user = craft()->userSession->getUser();

        $template->creatorId = $user->id;

        // if( !$postData['templateBody'] ){
        //     craft()->userSession->setError(Craft::t('Fill required fields.'));

        //     // Send the email back to the template
        //     craft()->urlManager->setRouteVariables(array(
        //         'template' => $template
        //     ));

        //     return;
        // }

        $template->getContent()->body = $postData['templateBody'];
        $template->getContent()->title = craft()->request->getPost('title', $template->title);

        if (craft()->aCPostman_template->saveTemplate($template))
        {
            craft()->userSession->setNotice(Craft::t('Email template saved.'));
            $this->redirectToPostedUrl($template);
        }
        else
        {
            craft()->userSession->setError(Craft::t('Could not save email template.'));

            // Send the email back to the template
            craft()->urlManager->setRouteVariables(array(
                'template' => $template
            ));
        }
    }
}