<?php

namespace Craft;

class ACPostmanController extends BaseController
{
    public function actionIndex()
    {
        $this->renderTemplate('acpostman');
    }

    public function actionEdit(array $variables = [])
    {

        if (empty($variables['email'])) {
            if (!empty($variables['emailId'])) {
                $variables['email'] = craft()->aCPostman->find($variables['emailId']);
                if (!$variables['email']) {
                    throw new HttpException(404);
                }
            } else {
                $variables['email'] = new ACPostmanModel();
            }
        }

        // Load tabs
        $variables['tabs'] = [];

        foreach ($variables['email']->getFieldLayout()->getTabs() as $index => $tab)
        {
            // Do any of the fields on this tab have errors?
            $hasErrors = false;

            if ($variables['email']->hasErrors())
            {
                foreach ($tab->getFields() as $field)
                {
                    if ($variables['email']->getErrors($field->getField()->handle))
                    {
                        $hasErrors = true;
                        break;
                    }
                }
            }

            $variables['tabs'][] = [
                'label' => $tab->name,
                'url' => '#tab'.($index+1),
                'class' => $hasErrors ? 'error' : null
            ];
        }

        /*
            This is used in send as element.
        */
        $settings = craft()->plugins->getPlugin('ACPostman')->getSettings();
        $variables['defaultFields'] = $settings->defaultFields;

        $this->renderTemplate('acpostman/_edit', $variables);
    }

    public function actionSettings()
    {
        $variables['defval'] = [];
        $sendAs = craft()->fields->getFieldByHandle('sendAs');
        $settings = craft()->plugins->getPlugin('ACPostman')->getSettings();

        foreach ($sendAs->settings['options'] as $key => $value) {
            if( $settings->defaultFields ){
                foreach( $settings->defaultFields as $key1 => $value1 ){
                    if( $value1['sendAs'] == $value['value'] ){
                        array_push( 
                            $variables['defval']
                            ,array(
                                'sendAs' => $value['value']
                                ,'fromName' => $value1['fromName']
                            )
                        );
                    }
                }
            }
            else{
                array_push( 
                    $variables['defval']
                    ,array(
                        'sendAs' => $value['value']
                        ,'fromName' => ''
                    )
                );
            }
        }

        $this->renderTemplate('acpostman/settings', $variables);
    }

    public function actionSendAndSave()
    {
        $this->requirePostRequest();

        $postData = craft()->request->getPost();

        if ($id = craft()->request->getPost('emailId'))
        {
            $email = craft()->aCPostman->find($id);

            if (!$email)
            {
                throw new Exception(Craft::t('No email exists with ID --> "{id}"', [
                    'id' => $id
                ]));
            }
        }
        else
        {
            $email = new ACPostmanModel();
        }

        // store custom fields data
        $email->setContentFromPost('fields');

        // Set creator and updater
        $user = craft()->userSession->getUser();

        $email->creatorId = $user->id;

        // Set status
        $email->status = 'sent';
        $email->templateId = !empty( craft()->request->getPost('templateId') ) ? craft()->request->getPost('templateId')[0] : 0;

        if (! $email->templateId)
        {
            $email->addError('templateId', 'Email type cannot be blank.');
        }

        if (! empty($email->getErrors()))
        {
            craft()->userSession->setError(Craft::t('Errors found. Please check the fields.'));
            craft()->urlManager->setRouteVariables([
                'email' => $email
            ]);

            return false;
        }

        if (craft()->aCPostman->save($email) && craft()->aCPostman->sendEmail($postData['fields']))
        {
            craft()->userSession->setNotice(Craft::t('Email saved and sent.'));
            $this->redirectToPostedUrl($email);
        }
        else
        {
            craft()->userSession->setError(Craft::t('Could not save email.'));

            // Send the email back to the template
            craft()->urlManager->setRouteVariables(array(
                'email' => $email
            ));
        }
    }

    public function actionShow(array $variables = [])
    {
        if (!empty($variables['emailId'])) {
            $variables['email'] = craft()->aCPostman->find($variables['emailId']);
            $variables['template'] = craft()->aCPostman_template->find($variables['email']['templateId']);
            if (!$variables['email']) {
                throw new HttpException(404);
            }
        } else {
            $variables['email'] = new ACPostmanModel();
        }

        // Load tabs
        $variables['tabs'] = [];

        foreach ($variables['email']->getFieldLayout()->getTabs() as $index => $tab)
        {
            // Do any of the fields on this tab have errors?
            $hasErrors = false;

            if ($variables['email']->hasErrors())
            {
                foreach ($tab->getFields() as $field)
                {
                    if ($variables['email']->getErrors($field->getField()->handle))
                    {
                        $hasErrors = true;
                        break;
                    }
                }
            }

            $variables['tabs'][] = [
                'label' => $tab->name,
                'url' => '#tab'.($index+1),
                'class' => $hasErrors ? 'error' : null
            ];
        }

        $this->renderTemplate('acpostman/_show', $variables);
    }

    public function actionGetEmailBody( )
    {
        $result = craft()->aCPostman->getElementById( $_GET['id'] );
        $this->returnJson(array('success' => true, 'body' => (string) $result->body ));
    }
}