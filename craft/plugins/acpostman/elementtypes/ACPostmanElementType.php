<?php
namespace Craft;

class ACPostmanElementType extends BaseElementType
{

    /**
     * Returns the element type name.
     *
     * @return string
     */
    public function getName()
    {
        return Craft::t('Postman Emails');
    }

    /**
     * Returns whether this element type has content.
     *
     * @return bool
     */
    public function hasContent()
    {
        return true;
    }

    /**
     * Returns whether this element type has titles.
     *
     * @return bool
     */
    public function hasTitles()
    {
        return false;
    }

    public function getSources($context=null)
    {
        $sources = [
            '*' => [
                'label' => Craft::t('All Sent Emails'),
            ]
        ];

        $sources[] = [
            'heading' => 'Email Type'
        ];

        $criteria = craft()->elements->getCriteria('ACPostman_Template');
        $criteria->group = 'postman';
        $templates = craft()->elements->findElements($criteria);

        foreach ($templates as $template)
        {
            $key = 'typeId:'.$template->id;

            $sources[$key] = [
                'label'    => $template->title,
                'criteria' => [
                    'typeId' => $template->id
                ]
            ];
        }

        return $sources;
    }

    /**
     * Returns the attributes that can be shown/sorted by in table views.
     *
     * @param string|null $source
     * @return array
     */
    public function defineTableAttributes($source = null)
    {
        return [
            'subject' => Craft::t('Subject'),
            'templateId' => Craft::t('Email Type'),
            'sendAs' => Craft::t('Sent As'),
            'fromName' => Craft::t('From Name'),
            'sendTo' => Craft::t('Sent To'),
            'creatorId' => Craft::t('Sent By'),
            'dateCreated' => Craft::t('Date Sent'),
        ];
    }

    /**
     * Defines the attributes that elements can be sorted by.
     *
     * @return array The attributes that elements can be sorted by.
     */
    public function defineSortableAttributes()
    {
        return [
            'subject' => 'Subject',
            'templateId' => 'Email Type',
            'sendAs' => 'Sent As',
            'fromName' => 'From Name',
            'sendTo' => 'Sent To',
            'creatorId' => 'Sent By',
        ];
    }

    public function getDefaultTableAttributes($source = null)
    {
        $attributes = [];

        if ($source == '*')
        {
            $attributes[] = 'subject';
            $attributes[] = 'templateId';
            $attributes[] = 'sendAs';
            $attributes[] = 'fromName';
            $attributes[] = 'sendTo';
            $attributes[] = 'creatorId';
            $attributes[] = 'dateCreated';
        }
        else
        {
            $attributes[] = 'subject';
            $attributes[] = 'sendAs';
            $attributes[] = 'fromName';
            $attributes[] = 'sendTo';
            $attributes[] = 'creatorId';
            $attributes[] = 'dateCreated';
        }

        return $attributes;
    }

    public function defineCriteriaAttributes()
    {
        return [
            'typeId' => AttributeType::Number,
        ];
    }

    /**
     * Modifies an element query targeting elements of this type.
     *
     * @param DbCommand             $query
     * @param ElementCriteriaModel  $criteria
     *
     * @return bool|false|null|void
     */
    public function modifyElementsQuery(DbCommand $query, ElementCriteriaModel $criteria)
    {
        $query
            ->addSelect('acpostman_emails.*')
            ->join('acpostman_emails acpostman_emails', 'acpostman_emails.elementId = elements.id');

        if ($criteria->typeId)
        {
            $query
                ->join('acpostman_templates acpostman_templates', 'acpostman_templates.id = acpostman_emails.templateId')
                ->andWhere(DbHelper::parseParam('acpostman_templates.id', $criteria->typeId, $query->params));
        }

        // echo $query->getText();
        // die;
    }


    public function getTableAttributeHtml(BaseElementModel $element, $attribute)
    {
        switch ($attribute)
        {
            case 'dateUpdated':
                return $element->dateUpdated->localeDate() .' '. $element->dateUpdated->localeTime();

            case 'templateId':
                $template = craft()->aCPostman_template->getTemplateById($element->templateId);

                return $this->_readableContent($template, 'acpostman/templates');

                break;

            case 'dateCreated':
                return $element->dateCreated->localeDate() .' '. $element->dateCreated->localeTime();

            case 'creatorId':
                if ($creator = craft()->users->getUserById($element->creatorId))
                {
                    return $this->_readableContent($creator, 'users', 'name');
                }

                return '';

            case 'status':
                if ( $element->status == 'pending' )
                {
                    return 'Pending';
                }
                else{
                    return 'Sent';
                }

                break;

            default:
                return parent::getTableAttributeHtml($element, $attribute);
        }
    }

    /**
     * Populates an element model based on a query result.
     *
     * @param array $row
     * @return ACPostmanModel
     */
    public function populateElementModel($row)
    {
        return ACPostmanModel::populateModel($row);
    }

    public function onBeforeDelete(){
        
    }

    public function getAvailableActions($source = null)
    {
        $user = craft()->userSession->getUser();

        $actions = [];

        if ($user->can('emailsDelete'))
        {
            // Delete
            $deleteAction = craft()->elements->getAction('Delete');
            $deleteAction->setParams([
                'confirmationMessage' => Craft::t('Are you sure you want to delete the selected emails?'),
                'successMessage'      => Craft::t('Emails deleted.'),
            ]);
            $actions[] = $deleteAction;
        }

        return $actions;
    }

    // Private Methods
    private function _readableContent($element, $path, $type='title')
    {
        if ($element == null)
        {
            return null;
        }
        else
        {
            $url = UrlHelper::getCpUrl($path.'/' . $element->id);

            $name = ($type == 'title') ? $element->getTitle() : $element->getName();

            return sprintf('<a href="%s" target="_blank">%s</a>', $url, $name);
        }
    }
}