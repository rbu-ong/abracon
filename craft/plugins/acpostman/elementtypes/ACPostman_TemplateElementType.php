<?php
namespace Craft;

class ACPostman_TemplateElementType extends BaseElementType
{

    /**
     * Returns the element type name.
     *
     * @return string
     */
    public function getName()
    {
        return Craft::t('ACPostman_Template');
    }

    /**
     * Returns whether this element type has content.
     *
     * @return bool
     */
    public function hasContent()
    {
        return true;
    }

    /**
     * Returns whether this element type has titles.
     *
     * @return bool
     */
    public function hasTitles()
    {
        return true;
    }

    public function getSources($context=null)
    {
        $sources = [
            '*' => [
                'label' => Craft::t('All Email Types'),
            ]
        ];

        $sources[] = [
            'heading' => 'Email Type'
        ];

        return $sources;
    }

    /**
     * Returns the attributes that can be shown/sorted by in table views.
     *
     * @param string|null $source
     * @return array
     */
    public function defineAvailableTableAttributes($source = null)
    {
        return [
            'title' => Craft::t('Title'),
            'creatorId' => Craft::t('Created By'),
            'dateCreated' => Craft::t('Date Created'),
        ];
    }

    /**
     * Defines the attributes that elements can be sorted by.
     *
     * @return array The attributes that elements can be sorted by.
     */
    public function defineSortableAttributes()
    {
        return [
            'title' => 'Title',
            'creatorId' => 'Created By',
        ];
    }

    public function defineCriteriaAttributes()
    {
        return [
            'typeId' => AttributeType::Number,
        ];
    }

    /**
     * Modifies an element query targeting elements of this type.
     *
     * @param DbCommand             $query
     * @param ElementCriteriaModel  $criteria
     *
     * @return bool|false|null|void
     */
    public function modifyElementsQuery(DbCommand $query, ElementCriteriaModel $criteria)
    {
        $query
            ->addSelect('acpostman_templates.*')
            ->join('acpostman_templates acpostman_templates', 'acpostman_templates.elementId = elements.id');
        // echo $query->getText();
    }

    public function getTableAttributeHtml(BaseElementModel $element, $attribute)
    {
        switch ($attribute)
        {
            case 'dateUpdated':
                return $element->dateUpdated->localeDate() .' '. $element->dateUpdated->localeTime();

            case 'dateCreated':
                return $element->dateCreated->localeDate() .' '. $element->dateCreated->localeTime();

            case 'creatorId':
                if ($creator = craft()->users->getUserById($element->creatorId))
                {
                    return $this->_readableContent($creator, 'users', 'name');
                }

                return '';

            default:
                return parent::getTableAttributeHtml($element, $attribute);
        }
    }

    public function getDefaultTableAttributes($source = null)
    {
        $attributes = [];

        $attributes[] = 'title';
        $attributes[] = 'creatorId';
        $attributes[] = 'dateCreated';

        return $attributes;
    }

    /**
     * Populates an element model based on a query result.
     *
     * @param array $row
     * @return ACPostmanModel
     */
    public function populateElementModel($row)
    {
        return ACPostman_TemplateModel::populateModel($row);
    }

    public function getAvailableActions($source = null)
    {

        // $actions[] = 'ACPostman_Template';
        $user = craft()->userSession->getUser();

        $actions = [];

        if ($user->can('emailsDelete'))
        {
            // Delete
            $deleteAction = craft()->elements->getAction('ACPostman_Template');
            $deleteAction->setParams([
                'confirmationMessage' => Craft::t('Are you sure you want to delete the selected email template?'),
                'successMessage'      => Craft::t('Email templates deleted.'),
                'alreadyUsedMessage'  => Craft::t('Some of the emails are not deleted because it is used in sent emails.'),
            ]);
            $actions[] = $deleteAction;
        }

        return $actions;
    }

    // Private Methods
    private function _readableContent($element, $path, $type='title')
    {
        if ($element == null)
        {
            return null;
        }
        else
        {
            $url = UrlHelper::getCpUrl($path.'/' . $element->id);

            $name = ($type == 'title') ? $element->getTitle() : $element->getName();

            return sprintf('<a href="%s" target="_blank">%s</a>', $url, $name);
        }
    }
}