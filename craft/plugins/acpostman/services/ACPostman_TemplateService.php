<?php
namespace Craft;

class ACPostman_TemplateService extends BaseApplicationComponent
{
    public function find($templateId)
    {
        return craft()->elements->getElementById($templateId, 'ACPostman_Template');
    }

    public function checkIfUsed( $templateIds )
    {

        $noEmails = array();

        foreach ($templateIds as $key => $value) {
            $query = craft()->db->createCommand()
                ->select('count(*) as numcounts')
                ->from('acpostman_emails')
                ->where(array( 'templateId' => $value ))
                ->queryRow();

            ( !$query['numcounts'] ) ? array_push( $noEmails , $value ) : null;
        }
        
        return $noEmails;
    }

    public function checkDuplicateTitle( $title, $id = 0 )
    {
        $query = craft()->db->createCommand()
                ->select('a.id')
                ->from('acpostman_templates as a')
                ->join( 'content as b','b.elementId = a.id' )
                ->where('b.title="'. $title .'"')
                ->andWhere(( $id ) ? ['not in', 'a.id', $id] : '1=1')
                ->queryRow();
                // ->getText();

        return count( $query['id'] );
    }

    public function getTemplateById($templateId)
    {
        return craft()->elements->getElementById($templateId, 'ACPostman_Template');
    }

    public function getAll($criteria = [])
    {
        return craft()->elements->getCriteria('ACPostman_Template', $criteria);
    }

    public function getSettings()
    {
        return craft()->plugins->getPlugin('ACPostman_Template')->getSettings();
    }

    public function saveTemplate(ACPostman_TemplateModel $model)
    {
        $isNew = ! $model->id;

        if (! $isNew)
        {
            $record = ACPostman_TemplateRecord::model()->findById($model->id);

            if (! $record)
            {
                throw new Exception(Craft::t('No email template exists with the ID "{id}"', [
                    'id' => $model->id
                ]));
            }
        }
        else
        {
            $record = new ACPostman_TemplateRecord;
        }

        $record->setAttributes($model->getAttributes(), false);

        if (! $record->validate())
        {
            // Attach errors to model
            $model->addErrors($record->getErrors());

            return false;
        }

        if (! $model->hasErrors())
        {
            $transaction = craft()->db->getCurrentTransaction() === null ? craft()->db->beginTransaction() : null;

            try
            {
                if (craft()->elements->saveElement($model))
                {
                    if ($isNew)
                    {
                        $record->id = $model->id;
                        $record->elementId = $model->id;
                    }

                    // Save to plugin table
                    $record->save(false);

                    if ($transaction !== null)
                    {
                        $transaction->commit();
                    }

                    return true;
                }
            }
            catch (\Exception $e)
            {
                if ($transaction !== null)
                {
                    $transaction->rollback();
                }

                throw $e;
            }

        }
    }
}