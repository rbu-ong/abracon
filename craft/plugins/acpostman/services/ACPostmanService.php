<?php
namespace Craft;

class ACPostmanService extends BaseApplicationComponent
{
    public function find($emailId)
    {
        return craft()->elements->getElementById($emailId, 'ACPostman');
    }

    public function getAll($criteria = [])
    {
        return craft()->elements->getCriteria('ACPostman', $criteria);
    }

    public function getSettings()
    {
        return craft()->plugins->getPlugin('ACPostman')->getSettings();
    }

    public function saveEmail( ACPostmanModel $email )
    {  
        $emailRecord = new ACPostmanRecord();

        $emailRecord->memberId = $email->memberId;
        $emailRecord->templateId = $email->templateId;
        $emailRecord->sendAs = $email->sendAs;
        $emailRecord->fromName = $email->fromName;
        $emailRecord->sendTo = $email->sendTo;
        $emailRecord->emailBody = $email->emailBody;
        $emailRecord->subject = $email->subject;

        $emailRecord->validate();
        $email->addErrors($emailRecord->getErrors());

        if (!$email->hasErrors())
        {
            $transaction = craft()->db->getCurrentTransaction() === null ? craft()->db->beginTransaction() : null;
            try
            {
                if (craft()->elements->saveElement($email))
                {
                    $emailRecord->id = $email->id;

                    $emailRecord->save(false);

                    if ($transaction !== null)
                    {
                        $transaction->commit();
                    }

                    return true;
                }
            }
            catch (\Exception $e)
            {
                if ($transaction !== null)
                {
                    $transaction->rollback();
                }

                throw $e;
            }
        }

        return false;
    }

    public function sendEmail($emailData)
    {
        $email = new EmailModel();

        /**
            params = array();
            just add here if you have other parameter or variable that
            need to be replace on body template.
        **/
        $params = array(
            'sendTo'
            ,'link'
        );

        foreach ($params as $key => $value) {
            if( $value != 'link' ){
                $emailData['emailBody'] = str_replace('{{' . $value . '}}', $emailData[$value], $emailData['emailBody']);
            }
            else{
                $emailData['emailBody'] = str_replace('{{' . $value . '}}', craft()->config->get('siteUrl') , $emailData['emailBody']);
            }
        }

        $email->fromEmail = $emailData['sendAs'];
        $email->fromName  = $emailData['fromName'];
        $email->toEmail   = $emailData['sendTo'];
        $email->sender    = $emailData['sendAs'];
        $email->subject   = $emailData['subject'];
        $email->body      = htmlspecialchars_decode($emailData['emailBody']);

        $variables['emailSubject'] = $emailData['subject'];

        return craft()->email->sendEmail($email, $variables);
    }

    public function save(ACPostmanModel $model)
    {
        $isNew = ! $model->id;

        if (! $isNew)
        {
            $record = ACPostmanRecord::model()->findById($model->id);

            if (! $record)
            {
                throw new Exception(Craft::t('No email exists with the ID "{id}"', [
                    'id' => $model->id
                ]));
            }
        }
        else
        {
            $record = new ACPostmanRecord;
        }

        $record->setAttributes($model->getAttributes(), false);


        /**
            params = array();
            just add here if you have other parameter or variable that
            need to be replace on body template.
        **/
        $params = array(
            'sendTo'
            ,'link'
        );

        $emailBody = $model->getContent()->emailBody;

        foreach ($params as $key => $value) {
            if( $value != 'link' ){
                $emailBody = str_replace('{{' . $value . '}}', $model->getContent()->$value, $emailBody);
            }
            else{
                $emailBody = str_replace('{{' . $value . '}}', craft()->config->get('siteUrl') , $emailBody);
            }
        }

        $model->getContent()->emailBody = htmlspecialchars_decode( $emailBody );

        if (! $record->validate())
        {
            // Attach errors to model
            $model->addErrors($record->getErrors());

            return false;
        }

        if (! $model->hasErrors())
        {
            $transaction = craft()->db->getCurrentTransaction() === null ? craft()->db->beginTransaction() : null;

            try
            {
                if (craft()->elements->saveElement($model))
                {
                    if ($isNew)
                    {
                        $record->id = $model->id;
                        $record->elementId = $model->id;
                    }

                    // Save to plugin table
                    $record->save(false);

                    if ($transaction !== null)
                    {
                        $transaction->commit();
                    }

                    return true;
                }
            }
            catch (\Exception $e)
            {
                if ($transaction !== null)
                {
                    $transaction->rollback();
                }

                throw $e;
            }

        }
    }

    public function getElementById($templateId)
    {
        return craft()->elements->getElementById($templateId, 'ACPostman_Template');
    }
}