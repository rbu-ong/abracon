<?php
namespace Craft;

class ACPostmanModel extends BaseElementModel
{
    protected $elementType = 'ACPostman';

    /**
     * Returns the element's CP edit URL.
     *
     * @return string|false
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('acpostman/' . $this->id);
    }

    /**
     * Set model attributes
     *
     * @return array
     */
    public function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), [
            'id' => AttributeType::Number,
            'memberId' => AttributeType::Number,
            'categoryId' => AttributeType::Number,
            'elementId' => [AttributeType::Number,
                'default' => 0,
            ],
            'status' => [AttributeType::String,
                'default' => 'pending',
            ],
            'templateId' => AttributeType::Number,
            'creatorId' => AttributeType::Number,
        ]);
    }

    public function getFieldLayout()
    {
        return craft()->fields->getLayoutByType('ACPostman');
    }

    /**
     * Returns the element's creator.
     *
     * @return UserModel|null
     */
    public function getCreator()
    {
        if ($this->creatorId)
        {
            return craft()->users->getUserById($this->creatorId);
        }
    }

    public function getTemplate()
    {
        if ($this->templateId)
        {
            return craft()->aCPostman_template->getTemplateById($this->templateId);
        }
    }

    public function __toString()
    {
        return $this->subject;
    }

}