<?php
namespace Craft;

class ACPostman_TemplateModel extends BaseElementModel
{
    protected $elementType = 'ACPostman_Template';

    /**
     * Returns the element's CP edit URL.
     *
     * @return string|false
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('acpostman/templates/' . $this->id);
    }

    /**
     * Set model attributes
     *
     * @return array
     */
    public function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), [
            'id' => AttributeType::Number,
            'elementId' => [AttributeType::Number,
                'default' => 0,
            ],
            'creatorId' => AttributeType::Number,
        ]);
    }
    
    /**
     * Returns the element's creator.
     *
     * @return UserModel|null
     */
    public function getCreator()
    {
        if ($this->creatorId)
        {
            return craft()->users->getUserById($this->creatorId);
        }
    }

    public function __toString()
    {
        return $this->getContent()->title;
    }

}