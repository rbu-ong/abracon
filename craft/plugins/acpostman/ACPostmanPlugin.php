<?php
namespace Craft;

class ACPostmanPlugin extends BasePlugin
{
    public function getName()
    {
        $pluginName = $this->getSettings()->getAttribute('customPluginName');

        return $pluginName ?: Craft::t('Postman');
    }

    public function getVersion()
    {
        return '1.0';
    }

    public function getDeveloper()
    {
        return 'AspenCore Ultimates';
    }

    public function getDeveloperUrl()
    {
        return 'http://ultimates.innovuze.com';
    }

    public function getDescription()
    {
        return 'A plugin to send or reply emails to users.';
    }

    public function hasCpSection()
    {
        return true;
    }

    public function getSettingsUrl()
    {
        return 'acpostman/settings';
    }

    public function registerCpRoutes()
    {
        return [
            'acpostman' => ['action' => 'ACPostman'],
            'acpostman/settings' => ['action' => 'ACPostman/settings'],
            'acpostman/new' => ['action' => 'ACPostman/edit'],
            'acpostman/(?P<emailId>\d+)' => ['action' => 'ACPostman/show'],
            'acpostman/templates' => ['action' => 'ACPostman_Template'],
            'acpostman/(newTemplate|templates/(?P<templateId>\d+))' => ['action' => 'ACPostman_Template/edit'],
        ];
    }

    public function prepSettings($settings)
    {

        $postData = craft()->request->getPost();

        // Delete the old field layout
        craft()->fields->deleteLayoutsByType('ACPostman');

        // Save the field layout
        $fieldLayout = craft()->fields->assembleLayoutFromPost();

        $fieldLayout->type = 'ACPostman';
        craft()->fields->saveLayout($fieldLayout);

        $settings['fieldLayout'] = $fieldLayout->id;
        $settings['defaultFields'] = $postData['defaultFields'];
        
        return $settings;
    }

    public function registerUserPermissions()
    {
        return array(
            'emailsDelete' => array('label' => Craft::t('Delete emails')),
        );
    }

    // ------
    // Protected methods
    // ---------------------------------------

    protected function defineSettings()
    {
        return [
            'customPluginName' => AttributeType::String,
            'fieldLayout' => AttributeType::Number,
            'requiredFields' => AttributeType::String,
            'defaultFields' =>AttributeType::Mixed
        ];
    }
}
