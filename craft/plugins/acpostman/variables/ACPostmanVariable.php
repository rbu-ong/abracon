<?php
namespace Craft;

class ACPostmanVariable
{
    public function getPluginName()
    {
        return craft()->plugins->getPlugin('ACPostman')->getName();
    }

    public function getSettings()
    {
        return craft()->aCPostman->getSettings();
    }

    public function getFieldLayout()
    {
        return craft()->fields->getLayoutByType('ACPostman');
    }

    public function getAllEmails()
    {
        
    }
}