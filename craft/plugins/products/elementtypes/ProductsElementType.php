<?php
/**
 * Products plugin for Craft CMS
 *
 * Products ElementType
 *
 * --snip--
 * Element Types are the classes used to identify each of these types of elements in Craft. There’s a
 * “UserElementType”, there’s an “AssetElementType”, and so on. If you’ve ever developed a custom Field Type class
 * before, this should sound familiar. The relationship between an element and an Element Type is the same as that
 * between a field and a Field Type.
 *
 * http://pixelandtonic.com/blog/craft-element-types
 * --snip--
 *
 * @author    DRL
 * @copyright Copyright (c) 2019 DRL
 * @link      https://abracon.com/
 * @package   Products
 * @since     1.0.0
 */

namespace Craft;

class ProductsElementType extends BaseElementType
{
    /**
     * Returns this element type's name.
     *
     * @return mixed
     */
    public function getName()
    {
        return Craft::t('Products');
    }

    /**
     * Returns whether this element type has content.
     *
     * @return bool
     */
    public function hasContent()
    {
        return true;
    }

    /**
     * Returns whether this element type has titles.
     *
     * @return bool
     */
    public function hasTitles()
    {
        return true;
    }

    /**
     * Returns whether this element type can have statuses.
     *
     * @return bool
     */
    public function hasStatuses()
    {
        return true;
    }

    /**
     * Returns whether this element type is localized.
     *
     * @return bool
     */
    public function isLocalized()
    {
        return false;
    }

    /**
     * Returns this element type's sources.
     *
     * @param string|null $context
     * @return array|false
     */
    public function getSources($context = null)
    {
        $sources = [
            '*' => [
                'label' => Craft::t('All Products'),
            ]
        ];

        return $sources;
    }

    /**
     * @inheritDoc IElementType::getAvailableActions()
     *
     * @param string|null $source
     *
     * @return array|null
     */
    public function getAvailableActions($source = null)
    {

        // $actions[] = 'ACPostman_Template';
        $user = craft()->userSession->getUser();

        $actions = [];

        // if ($user->can('emailsDelete'))
        // {
            // Delete
            $deleteAction = craft()->elements->getAction('Products');
            $deleteAction->setParams([
                'confirmationMessage' => Craft::t('Are you sure you want to delete the selected products?'),
                'successMessage'      => Craft::t('Product deleted.'),
                // 'alreadyUsedMessage'  => Craft::t('Some of the emails are not deleted because it is used in sent emails.'),
            ]);
            $actions[] = $deleteAction;
        // }

        return $actions;
    }

    /**
     * Returns the attributes that can be shown/sorted by in table views.
     *
     * @param string|null $source
     * @return array
     */
    public function defineAvailableTableAttributes($source = null)
    {
        return [
            'title' => Craft::t('Title'),
            'creatorId' => Craft::t('Created By'),
            'dateCreated' => Craft::t('Date Created'),
        ];
    }

    /**
     * Defines the attributes that elements can be sorted by.
     *
     * @return array The attributes that elements can be sorted by.
     */
    public function defineSortableAttributes()
    {
        return [
            'title' => 'Title',
            'creatorId' => 'Created By',
        ];
    }

    /**
     * Returns the attributes that can be shown/sorted by in table views.
     *
     * @param string|null $source
     * @return array
     */
    public function defineTableAttributes($source = null)
    {
        $attributes = [];

        $attributes[] = 'title';
        $attributes[] = 'creatorId';
        $attributes[] = 'dateCreated';

        return $attributes;
    }

    /**
     * Returns the table view HTML for a given attribute.
     *
     * @param BaseElementModel $element
     * @param string $attribute
     * @return string
     */
    public function getTableAttributeHtml(BaseElementModel $element, $attribute)
    {
        switch ($attribute)
        {
            case 'dateUpdated':
                return $element->dateUpdated->localeDate() .' '. $element->dateUpdated->localeTime();

            case 'dateCreated':
                return $element->dateCreated->localeDate() .' '. $element->dateCreated->localeTime();

            case 'creatorId':
                if ($creator = craft()->users->getUserById($element->creatorId))
                {
                    return $this->_readableContent($creator, 'users', 'name');
                }

                return '';

            default:
                return parent::defineTableAttributes($element, $attribute);
        }
    }

    /**
     * Defines any custom element criteria attributes for this element type.
     *
     * @return array
     */
    public function defineCriteriaAttributes()
    {
        return [
            'product_id' => AttributeType::Number,
        ];
    }

    /**
     * Modifies an element query targeting elements of this type.
     *
     * @param DbCommand $query
     * @param ElementCriteriaModel $criteria
     * @return mixed
     */
    public function modifyElementsQuery(DbCommand $query, ElementCriteriaModel $criteria)
    {
        $query
            ->addSelect('products.*')
            ->join('products products', 'products.elementId = elements.id');

        // echo $query->getText();
        // die;
    }

    /**
     * Populates an element model based on a query result.
     *
     * @param array $row
     * @return array
     */
    public function populateElementModel($row)
    {
        return ProductsModel::populateModel($row);
    }

    /**
     * Returns the HTML for an editor HUD for the given element.
     *
     * @param BaseElementModel $element
     * @return string
     */
    public function getEditorHtml(BaseElementModel $element)
    {
    }

    private function _readableContent($element, $path, $type='title')
    {
        if ($element == null)
        {
            return null;
        }
        else
        {
            $url = UrlHelper::getCpUrl($path.'/' . $element->id);

            $name = ($type == 'title') ? $element->getTitle() : $element->getName();

            return sprintf('<a href="%s" target="_blank">%s</a>', $url, $name);
        }
    }
}