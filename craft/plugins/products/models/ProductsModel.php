<?php
/**
 * Products plugin for Craft CMS
 *
 * Products Model
 *
 * --snip--
 * Models are containers for data. Just about every time information is passed between services, controllers, and
 * templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 * --snip--
 *
 * @author    DRL
 * @copyright Copyright (c) 2019 DRL
 * @link      https://abracon.com/
 * @package   Products
 * @since     1.0.0
 */

namespace Craft;

class ProductsModel extends BaseElementModel
{
    protected $elementType = 'Products';

    /**
     * Returns the element's CP edit URL.
     *
     * @return string|false
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('products/' . $this->id);
    }

    /**
     * Set model attributes
     *
     * @return array
     */
    public function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), [
            'id' => AttributeType::Number,
            'elementId' => [AttributeType::Number,
                'default' => 0,
            ],
            'creatorId' => AttributeType::Number,
            'datasheet' => AttributeType::Mixed,
        ]);
    }

    /**
     * Returns the element's creator.
     *
     * @return UserModel|null
     */
    public function getCreator()
    {
        if ($this->creatorId)
        {
            return craft()->users->getUserById($this->creatorId);
        }
    }

    public function __toString()
    {
        return $this->getContent()->title;
    }

}