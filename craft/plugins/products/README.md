# Products plugin for Craft CMS

Import and manage products plugin.

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install Products, follow these steps:

1. Download & unzip the file and place the `products` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /products`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `products` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

Products works on Craft 2.4.x and Craft 2.5.x.

## Products Overview

-Insert text here-

## Configuring Products

-Insert text here-

## Using Products

-Insert text here-

## Products Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [DRL](https://abracon.com/)
