<?php
/**
 * Products plugin for Craft CMS
 *
 * Products Translation
 *
 * @author    DRL
 * @copyright Copyright (c) 2019 DRL
 * @link      https://abracon.com/
 * @package   Products
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
);
