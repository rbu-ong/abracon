<?php
/**
 * Products plugin for Craft CMS
 *
 * Products Controller
 *
 * --snip--
 * Generally speaking, controllers are the middlemen between the front end of the CP/website and your plugin’s
 * services. They contain action methods which handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering post data, saving it on a model,
 * passing the model off to a service, and then responding to the request appropriately depending on the service
 * method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what the method does (for example,
 * actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 * --snip--
 *
 * @author    DRL
 * @copyright Copyright (c) 2019 DRL
 * @link      https://abracon.com/
 * @package   Products
 * @since     1.0.0
 */

namespace Craft;

class ProductsController extends BaseController
{

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     * @access protected
     */
    protected $allowAnonymous = array('actionIndex',
        );

    /**
     * Handle a request going to our plugin's index action URL, e.g.: actions/products
     */
    public function actionIndex()
    {
        $variables['template'] = craft()->products->getAll(['order' => 'sortOrder']);
        return $this->renderTemplate('products', $variables);
    }

    public function actionEdit(array $variables = [])
    {
        if (empty($variables['product'])) {
            if (!empty($variables['product_id'])) {
                $variables['product'] = craft()->products->find($variables['product_id']);
                if (!$variables['product']) {
                    throw new HttpException(404);
                }
            } else {
                $variables['product'] = new ProductsModel();
            }
        }

        $this->renderTemplate('products/_edit', $variables);
    }

    public function actionSave()
    {
        $this->requirePostRequest();
        $postData = craft()->request->getPost();
        if ($id = craft()->request->getPost('product_id'))
        {
            $product = craft()->products->find($id);

            if (!$product)
            {
                throw new Exception(Craft::t('No product exists with ID --> "{id}"', [
                    'id' => $id
                ]));
            }
        }
        else
        {
            $product = new ProductsModel();
        }

        // if( craft()->aCPostman_template->checkDuplicateTitle( craft()->request->getPost('title', $product->title), craft()->request->getPost('templateId') ) ){
        //     craft()->userSession->setError(Craft::t('Template title already exist.'));

        //     // Send the email back to the template
        //     craft()->urlManager->setRouteVariables(array(
        //         'product' => $product
        //     ));

        //     return;
        // }

        // store custom fields data
        $product->setContentFromPost('fields');

        // Set creator and updater
        $user = craft()->userSession->getUser();

        $product->creatorId = $user->id;
        $product->getContent()->body = $postData['product_desc'];
        $product->getContent()->title = craft()->request->getPost('title', $product->title);
        $product->datasheet = craft()->request->getPost('datasheet', $product->datasheet);

        if (craft()->products->saveProduct($product))
        {
            craft()->userSession->setNotice(Craft::t('Product template saved.'));
            $this->redirectToPostedUrl($product);
        }
        else
        {
            craft()->userSession->setError(Craft::t('Could not save product.'));

            // Send the email back to the template
            craft()->urlManager->setRouteVariables(array(
                'product' => $product
            ));
        }
    }

    public function actionImportCSV()
    {
        $file = \CUploadedFile::getInstanceByName('file');

        if( !is_null($file) ){
            $fileName = $_FILES["file"]["name"];

            // Get source
            $source = craft()->assetSources->getSourceTypeById(48);

            // Get folder to save to
            $folderId = craft()->assets->getRootFolderBySourceId(48);

            // Save file to Craft's temp folder for later use
            $fileName = AssetsHelper::cleanAssetName($file->name);
            $filePath = AssetsHelper::getTempFilePath($file->extensionName);
            $file->saveAs($filePath);

            $handle = fopen($filePath, 'r');
            $counter = 0;

            // print_r( fgetcsv($handle, 10000, ",") );
            // die;

            if ($_FILES["file"]["size"] > 0) {
                while (($column = fgetcsv($handle, 10000, ",")) !== FALSE) {
                    $product = new ProductsModel();

                    // Set creator and updater
                    $user = craft()->userSession->getUser();

                    $product->creatorId = $user->id;
                    $product->getContent()->body = $column[1];
                    $product->getContent()->title = $column[0];
                    craft()->products->saveProduct($product);
                }
            }

            fclose($handle);
        }
    }
}