<?php
namespace Craft;

/**
 * Delete Element Action
 *
 * @author    Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @copyright Copyright (c) 2014, Pixel & Tonic, Inc.
 * @license   http://craftcms.com/license Craft License Agreement
 * @link      http://craftcms.com
 * @package   craft.app.elementactions
 * @since     2.3
 */
class ProductsElementAction extends BaseElementAction
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritDoc IComponentType::getName()
     *
     * @return string
     */
    public function getName()
    {
        return Craft::t('Delete…');
    }

    /**
     * @inheritDoc IElementAction::isDestructive()
     *
     * @return bool
     */
    public function isDestructive()
    {
        return true;
    }

    /**
     * @inheritDoc IElementAction::getConfirmationMessage()
     *
     * @return string|null
     */
    public function getConfirmationMessage()
    {
        return $this->getParams()->confirmationMessage;
    }

    /**
     * @inheritDoc IElementAction::performAction()
     *
     * @param ElementCriteriaModel $criteria
     *
     * @return bool
     */
    public function performAction(ElementCriteriaModel $criteria)
    {
        // $ids = craft()->products->checkIfUsed( $criteria->ids() );

        // if( count( $ids ) ){
            craft()->elements->deleteElementById( $criteria->ids() );
        // }

        // if( count( $criteria->ids() ) == count( $ids ) ){
        //     $this->setMessage($this->getParams()->successMessage);
        // }
        // else{
        //     $this->setMessage($this->getParams()->alreadyUsedMessage);
        // }

        return true;
    }

    // Protected Methods
    // =========================================================================

    /**
     * @inheritDoc BaseElementAction::defineParams()
     *
     * @return array
     */
    protected function defineParams()
    {
        return array(
            'confirmationMessage' => array(AttributeType::String),
            'successMessage'      => array(AttributeType::String),
            // 'alreadyUsedMessage'  => array(AttributeType::String),
        );
    }
}
