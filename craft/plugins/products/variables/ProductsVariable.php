<?php
/**
 * Products plugin for Craft CMS
 *
 * Products Variable
 *
 * --snip--
 * Craft allows plugins to provide their own template variables, accessible from the {{ craft }} global variable
 * (e.g. {{ craft.pluginName }}).
 *
 * https://craftcms.com/docs/plugins/variables
 * --snip--
 *
 * @author    DRL
 * @copyright Copyright (c) 2019 DRL
 * @link      https://abracon.com/
 * @package   Products
 * @since     1.0.0
 */

namespace Craft;

class ProductsVariable
{
    /**
     * Whatever you want to output to a Twig template can go into a Variable method. You can have as many variable
     * functions as you want.  From any Twig template, call it like this:
     *
     *     {{ craft.products.exampleVariable }}
     *
     * Or, if your variable requires input from Twig:
     *
     *     {{ craft.products.exampleVariable(twigValue) }}
     */
    public function Products()
    {
        return craft()->elements->getCriteria('Products');
    }
}