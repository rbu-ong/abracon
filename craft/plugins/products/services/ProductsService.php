<?php
/**
 * Products plugin for Craft CMS
 *
 * Products Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    DRL
 * @copyright Copyright (c) 2019 DRL
 * @link      https://abracon.com/
 * @package   Products
 * @since     1.0.0
 */

namespace Craft;

class ProductsService extends BaseApplicationComponent
{   

    public function find($product_id)
    {
        return craft()->elements->getElementById($product_id, 'Products');
    }

    /**
     * This function can literally be anything you want, and you can have as many service functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     craft()->products->getAll()
     */
    public function getAll($criteria = [])
    {
        return craft()->elements->getCriteria('Products', $criteria);
    }

    public function saveProduct(ProductsModel $model)
    {
        $isNew = ! $model->id;

        if (! $isNew)
        {
            $record = ProductsRecord::model()->findById($model->id);

            if (! $record)
            {
                throw new Exception(Craft::t('No email template exists with the ID "{id}"', [
                    'id' => $model->id
                ]));
            }
        }
        else
        {
            $record = new ProductsRecord;
        }

        $record->setAttributes($model->getAttributes(), false);

        if (! $record->validate())
        {
            // Attach errors to model
            $model->addErrors($record->getErrors());
            return false;
        }

        if (! $model->hasErrors())
        {
            $transaction = craft()->db->getCurrentTransaction() === null ? craft()->db->beginTransaction() : null;

            try
            {
                if (craft()->elements->saveElement($model))
                {
                    if ($isNew)
                    {
                        $record->id = $model->id;
                        $record->elementId = $model->id;
                    }

                    // Save to plugin table
                    $record->save(false);

                    if ($transaction !== null)
                    {
                        $transaction->commit();
                    }
                    return true;
                }
            }
            catch (\Exception $e)
            {
                if ($transaction !== null)
                {
                    $transaction->rollback();
                }
                throw $e;
            }
        }
    }

}