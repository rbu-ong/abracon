<?php

return array(

    /**
     * How many static redirects to display in the Admin CP
     */
    "staticRedirectDisplayLimit"  => null,

    /**
     * How many dynamic redirects to display in the Admin CP
     */
    "dynamicRedirectDisplayLimit" => null,

);
