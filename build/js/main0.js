// Modal Triggers
$(".modal-trigger").click(function (e) {
  e.preventDefault();
  $($(this).data("modal")).addClass("showme");
});
$(".closeme").click(function () {
  $(this).parent().removeClass("showme");
});
$(".modal-card").click(function (e) {
  if (e.target !== this) {
    return;
  }
  $(this).removeClass("showme");
});

$(".slider").slick({
  dots: true
});