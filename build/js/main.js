$(document).ready(function () {

  if ($(".slider").length) {

    $('.slider').slick({
      nextArrow: '<img src="/assets/case-study/next.svg" alt="" class="slick-next">',
      prevArrow: '<img src="/assets/case-study/previous.svg" alt="" class="slick-prev">',
      infinite: false,
      dots: true
    });
    var item_length = $('.slider > div').length - 1;

    if ($('.slider').slick('slickCurrentSlide') == 0) {
      $('.slick-prev').hide();
    }
    $('.slider').on('afterChange', function () {

      if ($('.slider').slick('slickCurrentSlide') == 0) {
        $('.slick-prev').hide();
      } else if ($('.slider').slick('slickCurrentSlide') == $('.slider').slick("getSlick").slideCount - 1) {
        $('.slick-next').hide();
      } else {
        $('.slick-prev').show();
        $('.slick-next').show();
      }
    });
  }
});

$('select').on('change', function () {

  var menu = $("#" + this.value);

  if (!menu.hasClass("current")) {
    $(".current").hide().removeClass("current");
    menu.show().addClass("current");
  }
});

$('input').focus(function () {
  $(this).parent().find("label").addClass("has-input");
});
$('textarea').focus(function () {
  $(this).parent().find("label").addClass("has-input");
});

$(".nav-button, .mobile-close").click(function () {
  $("body").toggleClass("slide-left");
});

/** Screenshot scrolling **/

// this is the animation stuff


var $animation_elements = $('.animation-element');
var $static_element = $('.static-element');
var $top_element = $('.top-element');
var $window = $(window);

function check_if_at_top() {

  $.each($top_element, function (index) {
    var $element = $(this);
    var window_top_position = $window.scrollTop();
    var element_height = $element.outerHeight();
    var window_height = $window.height();
    var document_height = $(document).height();
    var window_bottom_position = window_top_position + window_height;
    var element_top_position = $element.offset().top;
    var element_bottom_position = element_top_position + element_height;

    if (window_top_position >= 290 && window_bottom_position <= document_height - 440) {

      $element.addClass("in-view").removeClass('hit-bottom');
    } else if (window_bottom_position >= document_height - 440) {
      $element.addClass("hit-bottom").removeClass('in-view');
    } else {
      $element.removeClass('in-view').removeClass('hit-bottom');
    }
  });
}

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = window_top_position + window_height;

  $.each($animation_elements, function () {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = element_top_position + element_height;

    //check to see if this current container is within viewport
    if (element_bottom_position >= window_top_position && element_top_position <= window_bottom_position) {

      $element.addClass('in-view');
    } else {
      $element.removeClass('in-view');
    }
  });
  $.each($static_element, function (index) {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = element_top_position + element_height;

    //check to see if this current container is within viewport
    if (element_bottom_position >= window_top_position && element_top_position <= window_bottom_position) {
      if ($element.hasClass("queue")) {
        $element.delay(index * 100).queue(function (next) {

          $element.addClass('in-view');
        });
      } else {
        $element.addClass('in-view');
      }
    } else {}
  });
}
check_if_in_view();
check_if_at_top();
$window.on('scroll resize', function () {
  window.requestAnimationFrame(check_if_in_view);
  window.requestAnimationFrame(check_if_at_top);
});

var nav_check = $(".nav .main-nav .search-btn");
$window.on('resize', function () {
  if (nav_check.css("display") == "inline-block") {
    $(".slide-left").removeClass("slide-left");
  }
});

$window.trigger('scroll');

var map;

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: new google.maps.LatLng(30.3141778, -97.698880),
    mapTypeId: 'roadmap',
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: true,
    styles: [{
      "elementType": "geometry",
      "stylers": [{
        "color": "#ebe3cd"
      }]
    }, {
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#523735"
      }]
    }, {
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#f5f1e6"
      }]
    }, {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#c9b2a6"
      }]
    }, {
      "featureType": "administrative.land_parcel",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "administrative.land_parcel",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#dcd2be"
      }]
    }, {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#ae9e90"
      }]
    }, {
      "featureType": "administrative.neighborhood",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dfd2ae"
      }]
    }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dfd2ae"
      }]
    }, {
      "featureType": "poi",
      "elementType": "labels.text",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#93817c"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#a5b076"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#447530"
      }]
    }, {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f5f1e6"
      }]
    }, {
      "featureType": "road",
      "elementType": "labels",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [{
        "color": "#fdfcf8"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f8c967"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#e9bc62"
      }]
    }, {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e98d58"
      }]
    }, {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#db8555"
      }]
    }, {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#806b63"
      }]
    }, {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dfd2ae"
      }]
    }, {
      "featureType": "transit.line",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#8f7d77"
      }]
    }, {
      "featureType": "transit.line",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#ebe3cd"
      }]
    }, {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dfd2ae"
      }]
    }, {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#b9d3c2"
      }]
    }, {
      "featureType": "water",
      "elementType": "labels.text",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#92998d"
      }]
    }]
  });
  var icons = {
    info: {
      icon: '/assets/header-logo.svg'
    }

  };

  function addMarker(feature) {
    var marker = new google.maps.Marker({
      position: feature.position,
      icon: icons[feature.type].icon,
      map: map
    });
  }
  var features = [{
    position: new google.maps.LatLng(30.261778, -97.698880),
    type: 'info'
  }];
  for (var i = 0, feature; feature = features[i]; i++) {
    addMarker(feature);
  }
}

if ($(window).scrollTop() > 20) {
  $(".nav").addClass("scrolling");
}

$(window).scroll(function () {
  if ($(".service-info").length) {
    servicesAnimation();
  }

  if ($(window).scrollTop() > 20) {
    $(".nav").addClass("scrolling");
  } else {
    $(".nav").removeClass("scrolling");
  }
});

function servicesAnimation() {

  if (!isElementInViewport($(".service-info")) && !$(".active-animation").length) {

    $(".locked:first").removeClass("locked").addClass("active-animation").addClass("unlocked");
    setTimeout(function () {
      $(".active-animation").removeClass("active-animation");
      servicesAnimation();
    }, 900);
  }
}

function isElementInViewport(el) {
  //special bonus for those using jQuery

  if (typeof jQuery === "function" && el instanceof jQuery) {
    el = el[0];
  }

  var rect = el.getBoundingClientRect();

  return rect.top >= 0 && rect.left >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
  rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
  ;
}

$(".search-btn").click(function () {
  $(".search").toggleClass("visible");
});
//
// $(".quote-slider").slick({
// 		nextArrow: '<img src ="/assets/icons/arrow-left.png" class = "right-slide-arrow">',
// 		prevArrow: '<img src ="/assets/icons/arrow-right.png" class = "left-slide-arrow">'
// 	});